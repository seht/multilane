# Multilane

## Consolidates application frameworks

### Set up multiple serverless apps with shared resources at highway speed.

Example configuration for app1 and app2 using Express and Koa.

```js
// config.js
apps: {
    all: {
        engines: {
            extensions: {
                html: 'nunjucks',
                hbs: 'handlebars'
            },
            default: 'html',
            options: {},
            requires: {
                nunjucks: require('nunjucks')
            }
        },
    },
    app1: {
        handlers: [
            require('express').static('public'),
            require("./middleware/exp/pre_routes"),
            require("./apps/app1/routes"),
            require("./middleware/exp/post_routes")
        ],
        views: [
            "views",
            "views/apps/app1"
        ],
        settings: {
            "trust proxy": 1
        }
    },
    app2: {
        handlers: [
            require('koa-static')('public'),
            require("./middleware/koa/pre_routes"),
            require("./apps/app2/routes"),
            require("./middleware/koa/post_routes")
        ],
        views: "views"
    }
}

```

```js
// index.js
const express = require('express')
const Koa = require('koa')

const apps = require('multilane')
const config = require('./config.js')['apps']

const app1 = apps.express(express(), require('consolidate'),
    { ...config['all'], ...config['app1'] },
    { 'example-context': { 'example-param': "args...", ping: () => { return 'pong' } } })

const app2 = apps.koa(new Koa(), require('koa-views'),
    { ...config['all'], ...config['app2'] },
    { 'example-context': { 'example-param': "args..." } })

app1.listen(3001, () => console.log("Started app1"))
app2.listen(3002, () => console.log("Started app2"))

```