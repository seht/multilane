class AppError extends Error {
    constructor(message, status) {
        super(message)
        this.name = this.constructor.name
    }
}

class RequestError extends AppError {
    constructor(message, status, request) {
        super(message, status)
        this.status = status || 500
        this.request = request.headers
    }
}

class NotFoundError extends RequestError {
    constructor(request, message = "Not Found") {
        super(message, 404, request)
    }
}

class BadRequestError extends RequestError {
    constructor(request, message = "Bad Request") {
        super(message, 400, request)
    }
}

module.exports = {
    AppError: AppError,
    RequestError: RequestError,
    NotFoundError: NotFoundError,
    BadRequestError: BadRequestError
}