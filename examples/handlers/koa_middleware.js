const errors = require('./errors')

const request = (ctx, next) => {
    console.log("context arguments", ctx['example-context'])
    return next()
}

const router = (ctx, next) => {
    throw new errors.NotFoundError(ctx.request)
}

const error = async (ctx, next) => {
    try {
        await next()
    } catch (error) {
        ctx.status = error.status || 500
        return ctx.render('error', { error })
    }
}

module.exports = { request, router, error }