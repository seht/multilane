const errors = require('./errors')

const request = (req, res, next) => {
    console.log("context arguments", req.app.get('example-context'))
    return next()
}

const router = (req, res, next) => {
    throw new errors.NotFoundError(req)
}

const error = (error, req, res, next) => {
    return res.status(error.status || 500)
        .render('error', { error })
}

module.exports = { request, router, error }