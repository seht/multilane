const KoaRouter = require('koa-router')
const router = new KoaRouter()

router.get('/', (ctx, next) => {
    const data = { title: 'App2 index page' }
    return ctx.render('apps/app2/index.html', { data })
})

module.exports = router.routes()