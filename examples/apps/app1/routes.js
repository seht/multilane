const express = require('express')
const router = express.Router()

router.get('/', (req, res) => {
    console.log("context ping...", req.app.get('example-context').ping())
    const data = { title: 'App1 index page' }
    return res.render('page', { data })
})

module.exports = router