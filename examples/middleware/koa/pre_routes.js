const middleware = require('../../handlers/koa_middleware')

module.exports = [
    middleware.request,
    middleware.error
]