const middleware = require('../../handlers/exp_middleware')

module.exports = [
    middleware.router,
    middleware.error
]