module.exports = {
    apps: {
        all: {
            engines: {
                extensions: {
                    html: 'nunjucks',
                    hbs: 'handlebars'
                },
                default: 'html',
                options: {},
                requires: {
                    nunjucks: require('nunjucks')
                }
            },
        },
        app1: {
            handlers: [
                require('express').static('public'),
                require("./middleware/exp/pre_routes"),
                require("./apps/app1/routes"),
                require("./middleware/exp/post_routes")
            ],
            views: [
                "views",
                "views/apps/app1"
            ],
            settings: {
                "trust proxy": 1
            }
        },
        app2: {
            handlers: [
                require('koa-static')('public'),
                require("./middleware/koa/pre_routes"),
                require("./apps/app2/routes"),
                require("./middleware/koa/post_routes")
            ],
            views: "views"
        }
    }
}