const express = require('express')
const Koa = require('koa')

const apps = require('../')
const config = require('./config.js')['apps']

const app1 = apps.express(express(), require('consolidate'),
    { ...config['all'], ...config['app1'] },
    { 'example-context': { 'example-param': "args...", ping: () => { return 'pong' } } })

const app2 = apps.koa(new Koa(), require('koa-views'),
    { ...config['all'], ...config['app2'] },
    { 'example-context': { 'example-param': "args..." } })

app1.listen(3001, () => console.log("Started app1"))
app2.listen(3002, () => console.log("Started app2"))