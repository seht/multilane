const helpers = require('../helpers')

/**
 *
 * @param {*} app Koa app instance.
 * @param {*} views require('koa-views').
 * @param {*} config Application configuration.
 * @param {*} context Application attributes to attach to app instance.
 */
const koa = (app, views, config = {}, context = {}) => {

    context = Object.assign(config.settings || {}, context)

    if (context) {
        for (const attr in context) {
            app.context[attr] = context[attr]
        }
    }

    if (views && config.views) {
        const opts = { map: {}, options: {} }
        if (config.engines) {
            if (config.engines.extensions) {
                opts.map = config.engines.extensions
            }
            if (config.engines.options) {
                opts.options = config.engines.options
            }
        }
        helpers.registerHandlers(app, views(config.views, opts))
    }

    if (config.handlers) {
        helpers.registerHandlers(app, config.handlers)
    }

    return app
}

module.exports = koa