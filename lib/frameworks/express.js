const helpers = require('../helpers')

/**
 * 
 * @param {*} app Express app instance.
 * @param {*} engines require('consolidate').
 * @param {*} config Application configuration.
 * @param {*} context Application attributes to attach to app instance.
 */
const express = (app, engines, config = {}, context = {}) => {

    context = Object.assign(config.settings || {}, context)

    if (context) {
        for (const attr in context) {
            app.set(attr, context[attr])
        }
    }

    if (config.views) {
        app.set('views', config.views)
    }

    if (engines && config.engines) {
        if (config.engines.default) {
            app.set('view engine', config.engines.default)
        }
        if (config.engines.requires) {
            for (const engine in config.engines.requires) {
                engines.requires[engine] = config.engines.requires[engine]
            }
        }
        if (config.engines.extensions) {
            for (const extname in config.engines.extensions) {
                if (config.engines.extensions[extname] in engines) {
                    app.engine(extname, engines[config.engines.extensions[extname]])
                }
            }
        }
    }

    if (config.handlers) {
        helpers.registerHandlers(app, config.handlers)
    }

    return app
}

module.exports = express