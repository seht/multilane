module.exports = {
    express: require('./frameworks/express'),
    koa: require('./frameworks/koa')
}