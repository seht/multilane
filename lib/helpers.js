const registerHandlers = (app, handlers) => {
    if (!Array.isArray(handlers)) {
        app.use(handlers)
    } else {
        for (const handler of handlers) {
            registerHandlers(app, handler)
        }
    }
}

module.exports = {
    registerHandlers: registerHandlers
}